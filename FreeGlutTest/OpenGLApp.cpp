#include "stdafx.h"
#include "OpenGLApp.h"
#include <GL/freeglut.h>
#include <vector>
#include "Square.h"
#define INITIAL_SIDE_LENGTH 100

using namespace OpenGLApp;


void OpenGLApp::openWindow(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
	glutInitWindowSize(800, 800);
	glutInitWindowPosition(150, 150);
}

void OpenGLApp::startApplication()
{
	static int depth;
	printf("Podaj liczbe zaglebien: ");
	scanf_s("%d", &depth);

	if (depth < 0) {
		printf("Nie mozesz podac liczby mniejszej od 0!\n");
		system("pause");
		exit(1);
	}

	int mainWindow = glutCreateWindow("Pierwsze Laboratorium");
	if (mainWindow == 0) {
		puts("Nie mozna stworzyc okna!!!\nWyjscie z programu.\n");
		exit(-1);
	}

	glutSetWindow(mainWindow);

	glutDisplayFunc([](void) {
		OpenGLApp::drawDefaultSceneCallback(OpenGLApp::buildSquaresToDraw(depth));
	});
	glutReshapeFunc(OpenGLApp::onResizeCallback);

	OpenGLApp::startOpenGL();

	glutMainLoop();
}

void OpenGLApp::drawDefaultSceneCallback(std::vector<Square> squares){
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	glLineWidth(5.0f); //grubosc 

	glBegin(GL_QUADS);


	glColor3f(0.0f, 0.0f, 2.0f); // kolor kwadratu
	glVertex2f(0.0, 0.0);
	glVertex2f(INITIAL_SIDE_LENGTH, 0.0);
	glVertex2f(INITIAL_SIDE_LENGTH, INITIAL_SIDE_LENGTH);
	glVertex2f(0.0, INITIAL_SIDE_LENGTH);

	glEnd();

	for (std::vector<Square>::iterator it = squares.begin(); it != squares.end(); ++it) {
		it->render();
	}

	glTranslatef(-50.f, 0, 0.0f);

	glBegin(GL_TRIANGLES);

	// Rysujemy tr�jk�ty
	glColor3f(1.0f, 0.0f, 0.0f); // Ustawiamy bie�acy kolor syowania na czerwony
	glVertex3f(0.0f, 50.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);  // Zmieniamy kolor rysowania na zielony
	glVertex3f(-50.0f, -50.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);  // Zmieniamy kolor rysowania na niebieski
	glVertex3f(50.0f, -50.0f, 0.0f);         // Prawy      

	glEnd();

	glFlush(); //wywolaj

}


void OpenGLApp::startOpenGL(void){
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //kolor tla
}

std::vector<Square> OpenGLApp::buildSquaresToDraw(int i) {
	float side_length = INITIAL_SIDE_LENGTH;
	std::vector<Square> squares;

	for (int counter = 0; counter < i; counter++)
	{
		side_length /= 3.0;
		float p_x = side_length,
			p_y = side_length;

		while (p_x < INITIAL_SIDE_LENGTH)
		{
			while (p_y < INITIAL_SIDE_LENGTH) //kolumny
			{

				/*
				Vertex mapping

				a = p_x
				b = p_y
				c = p_x + b
				d = p_y + b
				*/

				squares.push_back(Square(p_x, p_y, p_x + side_length, p_y + side_length));

				p_y = p_y + 3 * side_length;
			}
			p_x += +3 * side_length;
			p_y = side_length;
		}

	}

	return squares;
}

void OpenGLApp::onResizeCallback(int width, int height)
{
	int aspectRatio;

	if (height == 0) height = 1;

	aspectRatio = width / height;

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	if (width <= height)
		glOrtho(-100.0, 100.0, -100.0 / aspectRatio, 100.0 / aspectRatio, 1.0, -1.0);
	else
		glOrtho(-100.0*aspectRatio, 100.0*aspectRatio, -100.0, 100.0, 1.0, -1.0);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

}
