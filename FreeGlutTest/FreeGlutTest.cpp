﻿// FreeGlutTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "OpenGLApp.h"

using namespace std;

int main(const int argc, char **argv)
{
	OpenGLApp::openWindow(argc, argv);
	OpenGLApp::startApplication();

	return(0);
}


